#!/bin/sh

# Settings
###############################################################################

if [ -z "$GDDNS_API_KEY" ]; then 
  printf "error: GDDNS_API_KEY environment variable is required!\n"
  exit 1
fi

if [ -z "$GDDNS_DOMAIN" ]; then 
  printf "error: GDDNS_DOMAIN environment variable is required!\n"
  exit 1
fi

if [ -z "$GDDNS_RECORD_NAME" ]; then 
  printf "error: GDDNS_RECORD_NAME environment variable is required!\n"
  exit 1
fi

GDDNS_TTL="${GDDNS_TTL:-3600}"
GDDNS_EXTERNAL_IP="${GDDNS_EXTERNAL_IP:-"https://ipv4.icanhazip.com"}"

# Implementation
###############################################################################

# Discover external IP (via icanhazip.com by default)
if ! ip="$(curl --fail --silent "$GDDNS_EXTERNAL_IP")"; then
  printf "error: failed to get external IP address\n"
  exit 1
fi

# Do nothing if the IP has not changed since last time otherwise...
previous_ip=$(cat /tmp/DynGanDiNS.ip)
if [ "$ip" = "$previous_ip" ]; then
  exit 0 
fi
printf "info: new external IP %s\n" "$ip"

# ...update Gandi via the API...
request_data=$(cat <<EOF
{
    "rrset_values": ["$ip"],
    "rrset_ttl": $GDDNS_TTL
}
EOF
)
if ! curl --fail --silent \
     --request PUT \
     --header 'Content-Type: application/json' \
     --header "X-Api-Key:$GDDNS_API_KEY" \
     --data "$request_data" \
     https://dns.api.gandi.net/api/v5/domains/"$GDDNS_DOMAIN"/records/"$GDDNS_RECORD_NAME"/A; then
  printf "error: failed to update %s to point to %s\n" "$GDDNS_RECORD_NAME for $GDDNS_DOMAIN" "$ip"
  exit 1
fi
printf "success: %s now points to %s\n" "$GDDNS_RECORD_NAME for $GDDNS_DOMAIN" "$ip"

# ...and finally remember the IP that was last successfully sent 
# (to avoid sending again until it changes)
printf "%s" "$ip" > /tmp/DynGanDiNS.ip
