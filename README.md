# DynGanDiNS

Dynamic DNS for gandi.net

## How it works

* `curl` external IP (from [icanhazip.com](https://icanhazip.com) by default)
* `curl` to update the configured domain and record on [Gandi via the API](https://doc.livedns.gandi.net/)
* Unless the IP has not changed (compared to previous check at
/tmp/DynGanDiNS.ip)

## How to use

* Acquire an API key from Gandi by visiting your account page and finding the authorized apps UI to generate one.
* Clone this repo locally. 
* Update `Settings.env` with your API key, domain, record name and any optional setting overrides. 
* Run `make install` (sudo as needed). 

After installation the environment variables are sourced from `/etc/sysconfig/DynGanDiNS` which will look like this (but hopefully with your info by now, not empty variables):
```sh
GDDNS_API_KEY=
GDDNS_DOMAIN=
GDDNS_RECORD_NAME=
```

### Optional Settings

* `GDDNS_EXTERNAL_IP` defaults to `https://ipv4.icanhazip.com`
* `GDDNS_TTL` defaults to 3600 

Note that `make install` is to wire up `DynGanDiNS.sh` as a systemd service. It's possible you may want to just use the shell file in which case you'll have to supply the environment variables some other way or modify the file directly.

### Timer

If this is the first time setting everything up you may want to run the service right away instead of waiting for the timer. Ensure it worked by checking `journalctl -u DynGanDiNS`.
```sh
systemctl start DynGanDiNS
```
> After `make install` the settings live here: /etc/sysconfig/DynGanDiNS

By default the timer is configured to run the service every hour. Use `systemctl edit DynGanDiNS.timer` to override `OnCalendar` as desired.
```sh
systemctl enable --now DynGanDiNS.timer
```

## Why?

Internet service providers (ISPs) may or may not provide a static IP address for your connection to the World Wide Web. When they do not then it is _dynamic_ which poses a problem for domain name resolution (DNS) trying to associate the name with an IP that might change. To work around this some way of getting the current IP address and telling the DNS about it is required—ideally exactly and only when the IP changes.

This problem is very common for home computers wanting to expose services behind DNS to the larger network. For example a [Raspberry Pi](https://www.raspberrypi.org/) hosting an artisinal handcrafted website about knitting and gardening. Or maybe a [Wireguard VPN exposing an instance of Pi-hole for ad-block on the go!](https://docs.pi-hole.net/guides/vpn/wireguard/overview/) Or any other reason a server could be useful and you'd rather own instead of rent it.

It is highly likely that you will also need to [forward the ports](https://en.wikipedia.org/wiki/Port_forwarding) you'd like to use from your router to the computer running the service. This is because only the router talks to the internet—everything else *routes* through it! Usually traffic goes out but here we are handling traffic coming in. How to do this depends on your router and firmware, usually it's discoverable in the admin web GUI.

## Security

Be diligent with firewall and SSH/access controls when connecting any computer to the internet! How to do this is outside the scope of this project...do your research and be careful!

## See Also

* [Gandi LiveDNS API](https://doc.livedns.gandi.net/)
* [Systemd/Timers](https://wiki.archlinux.org/title/Systemd/Timers)
* [github.com/AdamVig/gandi-dynamic-dns](https://github.com/AdamVig/gandi-dynamic-dns)
* [DynDNS.com](https://dyndns.com)
* [no-ip.com](https://no-ip.com)
