install:
	install DynGanDiNS.sh /bin/DynGanDiNS
	install --target-directory /etc/systemd/system/ --mode 644 systemd/DynGanDiNS.service
	install --target-directory /etc/systemd/system/ --mode 644 systemd/DynGanDiNS.timer
	install --mode 644 -D Settings.env /etc/sysconfig/DynGanDiNS 

uninstall:
	rm /bin/DynGanDiNS
	rm /etc/systemd/system/DynGanDiNS.service
	rm /etc/systemd/system/DynGanDiNS.timer
	rm /etc/sysconfig/DynGanDiNS
